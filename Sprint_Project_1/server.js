const compression = require("compression")
const express = require('express');
const server = express();
const moment = require("moment");
const now = moment(new Date());

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
//configuracion de middleware para aceptar json en su formato 
server.use(express.json()) // for parsing application/json
server.use(express.urlencoded({ extended: true })) // for parsing application/x-   www-form-urlencoded
//configurar el option
const swaggerOptions =  {
    swaggerDefinition:{
        info: {
            title: 'Delilah Restó - My Primera API',
            version:'1.0.0'
        }
    },
    apis:['./server.js']
};
//configurar el swaggerDocs
const swaggerDocs = swaggerJsDoc(swaggerOptions);
//configuro el server para el uso del swagger
server.use('/api-docs', 
            swaggerUI.serve,
            swaggerUI.setup(swaggerDocs));



// ---------------------------------------------------- BASE DE DATOS ----------------------------------------------------

const users = [{
    idUser: 1,
    usuario: "admin",
    nombreComp: "Administrador",
    correo: "delilahfood@gmail.com",
    telefono: "2964332211",
    domicilio: "Perito Moreno 212",
    pass: "1234",
    admin: true,
    logueado: false,
    carrito: []
},{
    idUser: 2,
    usuario: "Juliansanta19",
    nombreComp: "Julian Santamaria",
    correo: "julian@santamaria.com",
    telefono: "2964332211",
    domicilio: "Guatemala 333",
    pass: "1234",
    admin: false,
    logueado: false,
    carrito: []
}];


const products = [{
    idProduct: 1,
    nombre: "Bagel de Salmón",
    precio: 350
},{
    idProduct: 2,
    nombre: "Hamburguesa Vegana",
    precio: 250
},{
    idProduct: 3,
    nombre: "Ensalada César",
    precio: 200
}];


const orders = [{
    idOrder: 0,
    carrito: "PRUEBA",
    totalPedido: "PRUEBA",
    formPago: "PRUEBA",
    horaPedido: "PRUEBA",
    estadoPedido: "PRUEBA",
    idUser: "PRUEBA",
    usuario: "PRUEBA",
    nombreComp: "PRUEBA",
    domicilio: "PRUEBA",   
    telefono: "PRUEBA",
    correo: "PRUEBA"
}];


const payMethod = [{
    idPayMethod: 1,
    nombre: "EFECTIVO" 
},{
    idPayMethod: 2,
    nombre: "DÉBITO"
},{
    idPayMethod: 3,
    nombre: "CRÉDITO"
},{
    idPayMethod: 4,
    nombre: "MERCADO PAGO"
}];


const orderStatus = [{
    idStatus: 1,
    nombreStatus: "PENDIENTE"
},{
    idStatus: 2,
    nombreStatus: "CONFIRMADO"
},{
    idStatus: 3,
    nombreStatus: "EN PREPARACIÓN"
},{
    idStatus: 4,
    nombreStatus: "ENVIADO"
},{
    idStatus: 5,
    nombreStatus: "ENTREGADO"
}];



// ---------------------------------------------------- MIDDLEWARES ----------------------------------------------------

const userLogueado = (req, res , next) =>{
    let id_user = req.params.id;
    let flag = false;
    users.forEach(user => {
        if(user.idUser == id_user){
            if(user.logueado == true){
                flag = true;
            };
        };
    });
    if(flag == false){
    res.json("USTED NO ESTÁ LOGUEADO");
    }else{
        next();
    };
};


const userAdmin = (req, res, next) =>{
    let id_user = req.params.id;
    let flag = false;
    users.forEach(user => {
        if(user.idUser == id_user){
            if(user.admin == true){
                flag = true;
            };
        };
    });
    if(flag == false){
    res.json("USTED NO ES ADMINISTRADOR");
    }else{
        next();
    };
};


    

// ---------------------------------------------------- USUARIOS ----------------------------------------------------

/**
 * @swagger
 * /usuarios/registro:
 *  post:
 *    description: Registra un usuario nuevo
 *    parameters:
 *    - name: usuario
 *      description: Usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: nombreComp
 *      description: Nombre Completo
 *      in: formData
 *      required: false
 *      type: string
 *    - name: correo
 *      description: Correo Electrónico
 *      in: formData
 *      required: false
 *      type: string
 *    - name: telefono
 *      description: Teléfono
 *      in: formData
 *      required: false
 *      type: string
 *    - name: domicilio
 *      description: Domicilio
 *      in: formData
 *      required: false
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */

server.post("/usuarios/registro",  (req, res) =>{
    let {usuario, nombreComp, correo, telefono, domicilio, pass} = req.body;
    const one_user = {
        idUser: users[users.length -1].idUser + 1,
        usuario: usuario,
        nombreComp: nombreComp,
        correo: correo,
        telefono: telefono,
        domicilio: domicilio,
        pass: pass,
        admin: false,
        logueado: false,
        carrito: []
    };

    if (nombreComp === "" || nombreComp === null) {
        return res.json("El nombre es inválido");
    }
        for(let i=0; i < users.length; i++) {
 
        let itemUsers = users[i]
 
    if(correo === "" || correo === null || itemUsers.correo === correo){
        return res.json("El correo es inválido o ya está en uso");
        }
    if (usuario === "" || usuario === null || itemUsers.usuario === usuario) {
        return res.json("El usuario es inválido o ya está en uso")
    }
}

    if (telefono === "" || telefono === null) {
        return res.json("El teléfono es inválido");
    }

    if (domicilio === "" || domicilio === null) {
        return res.json("El domicilio es inválido");
    }

    if (pass === "" || pass === null) {
        return res.json("La contraseña es inválida");
    }else{
        users.push(one_user);
        console.log(one_user)
        res.json("USUARIO CREADO CORRECTAMENTE");
    }  
});



/**
 * @swagger
 * /usuarios/login:
 *  post:
 *    description: Login de usuario
 *    parameters:
 *    - name: usuario
 *      description: Usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */


server.post('/usuarios/login', (req,res) =>{
    const {usuario, pass} = req.body;
    console.log(usuario,pass);
    let index = -1;
    users.forEach((user , i) => {
        if(user.usuario == usuario){
           if(user.pass == pass){
            index = i;
          }
        }
     });
    if(index == -1){
        res.json("Usuario o contraseña inválido");
    } else{
        users[index].logueado = true;
        console.log(users[index]);
        res.json(`Bienvenido ${users[index].nombreComp}`);
    }
})

// ---------------------------------------------------- PRODUCTOS ----------------------------------------------------

/**
 * @swagger
 * /productos/{idUser}:
 *  get:
 *    description: Listado de productos
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

 server.get('/productos/:id', userLogueado, (req,res)=>{
    res.json(products);
});


/**
 * @swagger
 * /productos/alta_productos/{idUser}:
 *  post:
 *    description: Alta de Productos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nombre del Producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: precio
 *      description: Precio del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */


 server.post('/productos/alta_productos/:id', userLogueado, userAdmin, (req,res)=>{
        let {nombre, precio} = req.body;
            const one_product = {
                idProduct: products[products.length -1].idProduct + 1,
                nombre: nombre,
                precio: parseInt(precio),
};

    if (!req.body.nombre || !req.body.precio) {
        
        res.json("DEBE COMPLETAR TODOS LOS CAMPOS");

    }else{

        products.push(one_product);
        console.log(products);
        res.json("PRODUCTO DADO DE ALTA CORRECTAMENTE");

    }

});


/**
 * @swagger
 * /productos/modif_productos/{idUser}:
 *  put:
 *    description: Modificación de Productos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nuevo Nombre del Producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: precio
 *      description: Nuevo Precio del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */


server.put('/productos/modif_productos/:id', userLogueado, userAdmin,(req,res)=>{

        let i = products.findIndex(product => product.idProduct == parseInt(req.body.idProduct));

        if(i != -1){
            products[i].nombre = req.body.nombre;
            products[i].precio = parseInt(req.body.precio);

            if (!req.body.nombre || !req.body.precio) {
        
                res.json("DEBE COMPLETAR TODOS LOS CAMPOS");

        }else{
            res.json(products[i]);
            console.log(products[i]);

        }

        }else{
            res.json("NO SE HA ENCONTRADO EL PRODUCTO");
        }
    });


/**
 * @swagger
 * /productos/elim_productos/{idUser}:
 *  delete:
 *    description: Eliminar Productos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

server.delete('/productos/elim_productos/:id', userLogueado, userAdmin, (req,res)=>{

    let i = products.findIndex(product => product.idProduct === parseInt(req.body.idProduct));

    if(i != -1){
        if (!req.body.idProduct) {
        
            res.json("DEBE COMPLETAR EL ID A ELIMINAR");
    
        }else{

        products.splice(i, 1);
        res.json("PRODUCTO ELIMINADO");
        console.log(products);

        }

    }else{

        res.json("NO SE HA ENCONTRADO EL PRODUCTO");
    }

});



// ---------------------------------------------------- PEDIDOS ----------------------------------------------------


/**
 * @swagger
 * /pedidos/carrito/agregar/{idUser}:
 *  post:
 *    description: Agregar Productos al Carrito
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: cant
 *      description: Cantidad del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

 server.post('/pedidos/carrito/agregar/:id', userLogueado, (req,res)=>{

    let i = products.findIndex(product => product.idProduct == parseInt(req.body.idProduct));
    let u = users.findIndex(user => user.idUser == parseInt(req.params.id))

    if(i != -1){
    const itemCarrito = {
        idProduct: parseInt(req.body.idProduct),
        nombreProd: products[i].nombre,
        precio: products[i].precio * req.body.cant,
        cant: parseInt(req.body.cant),
    }

    if (!req.body.idProduct || !req.body.cant) {
        
        res.json("DEBE COMPLETAR TODOS LOS CAMPOS");

    }else{

    users[u].carrito.push(itemCarrito);
    res.json(users[u].carrito); //CARRITO ACTUALIZADO

    }

    }else{

        res.json("PRODUCTO NO DISPONIBLE");

    };

 });


/**
 * @swagger
 * /pedidos/carrito/eliminar/{idUser}:
 *  delete:
 *    description: Eliminar Productos del Carrito
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

 server.delete('/pedidos/carrito/eliminar/:id', userLogueado, (req,res)=>{

    let u = users.findIndex(user => user.idUser == parseInt(req.params.id));

    let i = users[u].carrito.findIndex(product => product.idProduct === parseInt(req.body.idProduct));


    if(i != -1){
        if (!req.body.idProduct) {
        
            res.json("DEBE COMPLETAR TODOS LOS CAMPOS");
    
        }else{
    
        users[u].carrito.splice(i, 1);
        res.json(users[u].carrito);

        };

    }else{

        res.json("NO SE HA ENCONTRADO EL PRODUCTO EN SU CARRITO");
    }
});


/**
 * @swagger
 * /pedidos/hacer_pedido/{idUser}:
 *  post:
 *    description: Hacer un Pedido
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: formPago
 *      description: Indicar Forma de Pago (Índice)
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

server.post('/pedidos/hacer_pedido/:id', userLogueado, (req,res)=>{

    let u = users.findIndex(user => user.idUser == parseInt(req.params.id));

    let totalPedido = 0

    users[u].carrito.forEach(product => totalPedido += parseInt(product.precio));
    
    if(users[u].carrito.length != 0){

    const one_order = {
        idOrder: orders[orders.length -1].idOrder + 1,
        carrito: users[u].carrito,
        totalPedido: totalPedido,
        formPago: payMethod[req.body.formPago].nombre,
        horaPedido: now.format('MMMM Do YYYY, h:mm:ss a'),
        estadoPedido: orderStatus[0].nombreStatus,
        idUser: parseInt(req.params.id),
        usuario: users[u].usuario,
        nombreComp: users[u].nombreComp,
        domicilio: users[u].domicilio,
        telefono: users[u].telefono,
        correo: users[u].correo
    };
        orders.push(one_order);
        users[u].carrito = [];
        res.json(one_order); //SU PEDIDO SE ESTÁ PREPARANDO

    }else{

        res.json("SU CARRITO ESTÁ VACÍO"); 

    };
  
  });


/**
 * @swagger
 * /pedidos/mis_pedidos/{idUser}:
 *  get:
 *    description: Pedidos realizados por el Usuario
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

  server.get('/pedidos/mis_pedidos/:id', userLogueado, (req,res)=>{

    let mis_pedidos = orders.filter(order => order.idUser == parseInt(req.params.id));

    if(mis_pedidos){

        res.json(mis_pedidos);

    }else{
        res.json("USTED AÚN NO HA REALIZADO NINGÚN PEDIDO");

    };
});


/**
 * @swagger
 * /pedidos/historial_pedidos/{idUser}:
 *  get:
 *    description: Todos los Pedidos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

server.get('/pedidos/historial_pedidos/:id', userLogueado, userAdmin, (req,res)=>{

    res.json(orders);


});


/**
 * @swagger
 * /pedidos/actualizar_estado/{idUser}:
 *  put:
 *    description: Actualizar Estado de los Pedidos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idOrder
 *      description: ID del Pedido
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: estadoPedido
 *      description: Nuevo Estado del Pedido (Índice)
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

server.put('/pedidos/actualizar_estado/:id', userLogueado, userAdmin,(req,res)=>{

    let i = orders.findIndex(order => order.idOrder == parseInt(req.body.idOrder));

    if(i != -1){

        if (!req.body.idOrder) {
        
            res.json("DEBE COMPLETAR EL ID DEL PEDIDO");
    
        }else{
    
            orders[i].estadoPedido = orderStatus[req.body.estadoPedido].nombreStatus; 
            // 0= Pendiente - 1= Confirmado - 2= En Preparación - 3= Enviado - 4= Entregado 
            res.json(`Estado de Producto modificado a: ${orderStatus[req.body.estadoPedido].nombreStatus}`);
            console.log(orders);

        };

    }else{

        res.json("NO SE HA ENCONTRADO LA ORDEN");
    }
});



// ---------------------------------------------------- MEDIOS DE PAGO ----------------------------------------------------


/**
 * @swagger
 * /medios_pago/{idUser}:
 *  get:
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    description: Ver Todos los Medios de Pago Habilitados
 *    responses:
 *      200:
 *        description: Success * 
 */

server.get('/medios_pago/:id', userLogueado, userAdmin,(req,res)=>{
    res.json(payMethod);
});


/**
 * @swagger
 * /medios_pago/alta_medios/{idUser}:
 *  post:
 *    description: Alta de Medios de Pago (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nombre del Nuevo Medio de Pago
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */

server.post('/medios_pago/alta_medios/:id', userLogueado, userAdmin, (req,res)=>{

    const one_payMethod = {
            idPayMethod: payMethod[payMethod.length -1].idPayMethod + 1,
            nombre: (req.body.nombre).toUpperCase(),
    };

    if (!req.body.nombre) {
        
        res.json("DEBE COMPLETAR EL NOMBRE DEL NUEVO MEDIO DE PAGO");

    }else{

    payMethod.push(one_payMethod);
    console.log(payMethod);
    res.json(payMethod);

    };

});


/**
 * @swagger
 * /medios_pago/modificar_medios/{idUser}:
 *  put:
 *    description: Modificación de Medios de Pago (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idPayMethod
 *      description: ID del Medio de Pago
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nuevo Nombre del Medio de Pago
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */

server.put('/medios_pago/modificar_medios/:id', userLogueado, userAdmin, (req,res)=>{

    let i = payMethod.findIndex(method => method.idPayMethod == parseInt(req.body.idPayMethod));

    if(i != -1){
            
        if (!req.body.idPayMethod) {
        
            res.json("DEBE COMPLETAR EL ID DEL MEDIO DE PAGO");
        
    }else{
        
        payMethod[i].nombre = (req.body.nombre).toUpperCase();

        res.json("MEDIO DE PAGO MODIFICADO");

        console.log(payMethod);

    };

        }else{

            res.json("NO SE HA ENCONTRADO EL MEDIO DE PAGO");
        };
    });


/**
 * @swagger
 * /medios_pago/eliminar_medios/{idUser}:
 *  delete:
 *    description: Eliminar Medio de Pago (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idPayMethod
 *      description: ID del Medio de Pago
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

server.delete('/medios_pago/eliminar_medios/:id', userLogueado, userAdmin, (req,res)=>{

    let i = payMethod.findIndex(method => method.idPayMethod === parseInt(req.body.idPayMethod));

    if(i != -1){

        if (!req.body.idPayMethod) {
        
            res.json("DEBE COMPLETAR TODOS LOS CAMPOS");
    
        }else{
    
        payMethod.splice(i, 1);

        res.json("MEDIO DE PAGO ELIMINADO");
        console.log(payMethod);

        };

    }else{

        res.json("MEDIO DE PAGO NO ENCONTRADO");
    };
});


// ---------------------------------------------------- PUERTO ----------------------------------------------------

server.listen(3000, (req,res)=>{
    console.log("Corriendo servidor por el puerto 3000");
});

