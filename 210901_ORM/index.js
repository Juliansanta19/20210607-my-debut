const {Sequelize, DataTypes, Model} = require("sequelize");

const sequelize = new Sequelize ("acamicadb", "root", "", {
    host: "localhost",
    dialect:"mysql",
})

//verificamos que conecte a la base de datos

sequelize
.authenticate()
.then(() => {
    console.log("Conexión satisfactoria")
})
.catch(() =>{
    console.log("Conexión con problemas")
})

class Usuarios extends Model {}

Usuarios.init(
    {
        nombre: DataTypes.STRING,
        apellidos: DataTypes.STRING,
    },
    {
        sequelize,
        modelName: "usuarios",
        timestamps: false, //para remover las columnas de createdAt y updatedAt
    }
);


class Casas extends Model {}

Casas.init(
    {
        nombre: DataTypes.STRING,
    },
    {
        sequelize,
        modelName: "casas",
        timestamps: false
    }
);

//asociación usando belongsTo
//

Usuarios.belongsTo(Casas, {foreignKey: "id_casa"});

//
//
//

(async () => {

    //insertar datos

    await sequelize.sync({force:true});

    const datoPlaya = { nombre: "casa de la playa"};
    const dataInsertCasa = await Casas.create(datoPlaya);
    console.log(dataInsertCasa.toJSON());

    const datos = { nombre: "Josefa", apellidos: "Carolina", id_casa: 1};
    const usuarioData = await Usuarios.create(datos);
    console.log(usuarioData.toJSON());

    //consulta mostrará el último ingresado

    console.log(usuarioData.nombre);
    console.log(usuarioData.apellidos);

    //actualizamos sus valores

    usuarioData.nombre = "Manuela"
    usuarioData.apellidos = "Jiménez Update";

    //guardamos los cambios para actualizar

    await usuarioData.save();

    //volvemos a consultar la dataset

    console.log(usuarioData.nombre); //Manuela
    console.log(usuarioData.apellidos); //Jiménez Update

    // eliminamos el registro
    //await usuarioData.destroy()

    //Buscar un solo resultado

    const userOne = await Usuarios.findOne({
        where:{
            nombre: "Manuela",
        },
    });

    if (userOne === null) {
        console.log("Usuario no encontrado")
    }else{
        console.log("Usuario Encontrado", userOne.toJSON());
    }

    //buscar varios

    //buscar muchos resulatdos

    const usuarios = await Usuarios.findAll({
        where: {
            nombre: "Manuela",
        },
    });

    usuarios.forEach((item) => {
        console.log(item.toJSON()); //listo, todos los resultados
    });


    let usuarioPlaya = await Usuarios.findAll({
        include: {
            model: Casas
        },

        attributes: ["nombre", "apellidos"]
    });

    usuarioPlaya.forEach((item) => {
        console.log("datos playa", item.toJSON()); //listo, todos los resultados
    });


})();