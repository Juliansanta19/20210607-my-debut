const mongoose = require('./mongodb');
const express = require('express');
const app = express();
app.listen(3000, function () {
    console.log('listening on 3000')
})
const userSchema = mongoose.Schema({
    nombre:String,
    apellido:String,
    edad:Number,
});
const usuarios = mongoose.model("Usuarios", userSchema);
try {
    usuarios.deleteOne({_id:'611d7b1cdc42f13e70d600a0'}).then((result,error) =>{
        console.log('usuario eliminado',result);
        if(error){
            throw new Error(error.message);
        }
    });
}catch (err) {
    console.log(err);
}