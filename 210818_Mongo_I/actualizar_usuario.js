const mongoose = require('./mongodb');
const express = require('express');
const app = express();
app.listen(3000, function () {
    console.log('listening on 3000')
})
const userSchema = mongoose.Schema({
    nombre:String,
    apellido:String,
    edad:Number,
});
const usuarios = mongoose.model("Usuarios", userSchema);
usuarios.findOne({_id:'611d7b1cdc42f13e70d6009f'}).then((result) =>{
    result.nombre = "Pedro";
    result.apellido = "Nel";
    result.edad = 50;
    result.save();
    console.log(result);
});
