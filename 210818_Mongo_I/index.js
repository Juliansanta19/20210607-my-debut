const mongoose = require('./mongodb');
const express = require('express');
const app = express();
app.listen(3000, function () {
    console.log('listening on 3000')
})
const userSchema = mongoose.Schema({
    nombre:String,
    apellido:String,
    edad:Number,
});
//insertar uno nuevo usuario
const Usuarios = mongoose.model("Usuarios", userSchema);
//listar usuario
Usuarios.find().then((result) => console.log(result));