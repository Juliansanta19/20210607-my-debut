const express = require('express');
const server = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
//configuracion de middleware para aceptar json en su formato 
server.use(express.json()) // for parsing application/json
server.use(express.urlencoded({ extended: true })) // for parsing application/x-   www-form-urlencoded
//configurar el option
const swaggerOptions =  {
    swaggerDefinition:{
        info: {
            title: 'Acamica API',
            version:'1.0.0'
        }
    },
    apis:['./server.js']
};
//configurar el swaggerDocs
const swaggerDocs = swaggerJsDoc(swaggerOptions);
//configuro el server para el uso del swagger, o configurar el endpoint
server.use('/api-docs', 
            swaggerUI.serve,
            swaggerUI.setup(swaggerDocs));
/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
 *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success * 
 */
server.post('/estudiantes', (req, res) => {
    console.log(req.body);
    res.status(201).send('hola soy un post');
});  
    
/**
 * @swagger
 * /estudiantes:
 *  get:
 *    description: Crea un nuevo estudiante 
 *    responses:
 *      200:
 *        description: Success * 
 */
server.get('/estudiantes', (req, res) => {
    res.status(201).send('hola estudiantes xxxx');
    });  
    
/**
 * @swagger
 * /estudiantes:
 *  delete:
 *    description: Elimina un estudiante,Elimina un estudiante ,Elimina un estudiante  ,Elimina un estudiante Elimina un estudiante Elimina un estudiante Elimina un estudiante Elimina un estudiante Elimina un estudiante  
 *    responses:
 *      200:
 *        description: Success * 
 */
 server.delete('/estudiantes', (req, res) => {
    res.status(201).send('hola estudiante eliminado satisfactoriamente');
    }); 
    
    
/**
 * @swagger
 * /estudiantes:
 *  patch:
 *    description: Actualizar un estudiante
 *    responses:
 *      200:
 *        description: Success * 
 */
 server.patch('/estudiantes', (req, res) => {
    res.status(201).send('hola estudiante actualizado satisfactoriamente');
    }); 
/**
 * @swagger
 * /estudiantes:
 *  put:
 *    description: Actualizar un estudiante
 *    responses:
 *      200:
 *        description: Success * 
 */
 server.put('/estudiantes', (req, res) => {
    res.status(201).send('hola estudiante actualizado satisfactoriamente');
    }); 
server.listen(3000, function(){
    console.log('Estoy escuchando por el puerto 3000');
});