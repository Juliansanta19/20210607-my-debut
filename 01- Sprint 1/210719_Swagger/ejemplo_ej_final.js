const express = require('express');
const server = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

//Configurar los Middleware
//Este método se llama como middleware
server.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
server.use(express.urlencoded({ extended: true }));

//CONFIGURAR SWAGGER
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Tarea final 13ra clase Acamica',
        version: '1.0.0'
      }
    },
    apis: ['./foro.js'],
  };

//CONFIGURAR EL SwaggerDocs
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

//CONFIGURAR SERVER PARA EL USO DE SWAGGER, O CONFIGURAR EL ENDPOINT

server.use('/api-docs',
   swaggerUI.serve,
   swaggerUI.setup(swaggerDocs));

//ARRAY USUARIOS

let usuarios = [{
    id: 1,
    nombre: 'Alex',
    apellido: 'Fank',
    email: 'alex@fank.com.ar'
}];
let usuario = {
id: "",
nombre: "",
apellido: "",
email: ""
};

//ARRAY TOPICOS

let topicos = [];
let topico = {
id: "",
titulo: "",
descripcion: "",
};

//ARRAY COMENTARIOS

let comentarios = [];
let comentario = {
id: "",
topico_id: "",
user_id: "",
comentario: "",
};

//ENDPOINTS USUARIOS

/**
 * @swagger
 * /usuarios/alta:
 *  post:
 *    description: Crea un nuevo usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.post('/usuarios/alta',(req,res)=>{
    const {id, nombre, apellido, email} = req.body;
    const usuario = {
        id: id,
        nombre: nombre,
        apellido: apellido,
        email: email
    };
    usuarios.push(usuario);
    console.log('Usuario creado exitosamente');
    return res.send(usuario);
})

/**
 * @swagger
 * /usuarios/mod:
 *  put:
 *    description: Modifica usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.put('/usuarios/mod/',(req,res)=>{
    const {id, nombre, apellido, email} = req.body;
    const user_mod = {
        id: id,
        nombre: nombre,
        apellido: apellido,
        email: email
    };
    let user_id = user_mod.id;
    usuarios.forEach(usuario => {
        if(usuario.id = user_id){
            usuario.id = user_mod.id;
            usuario.nombre = user_mod.nombre;
            usuario.apellido = user_mod.apellido;
            usuario.email = user_mod.email;
        }
        console.log('Usuario modificado exitosamente');
        return res.send(usuario);
    });
})

/**
 * @swagger
 * /usuarios/del:
 *  delete:
 *    description: Elimina un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 server.delete('/usuarios/del/',(req,res)=>{
    const {id} = req.body;
    
    let user_id = req.body.id;
    usuarios.forEach((usuario,i) => {
        if(usuario.id = user_id){
            usuarios.splice(i,1)
            console.log('Usuario eliminado exitosamente');
            return res.send('Usuario eliminado.');
        }
    });
})

/**
 * @swagger
 * /usuarios:
 *  get:
 *    description: Devuelve la lista de los usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.get('/usuarios', (req,res)=>res.send(usuarios));

//ENDPOINTS TOPICOS

/**
 * @swagger
 * /topicos/alta:
 *  post:
 *    description: Crea un nuevo topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: titulo
 *      description: Titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Descripción del topico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */
server.post('/topicos/alta',(req,res)=>{
    const {id, titulo, descripcion} = req.body;
    const topico = {
        id: id,
        titulo: titulo,
        descripcion: descripcion
    };
    topicos.push(topico);
    return res.send(topico);
    // console.log('Topico creado exitosamente');
})

/**
 * @swagger
 * /topicos/mod:
 *  put:
 *    description: Modifica topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: titulo
 *      description: Titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Descripción del topico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */

server.put('/topicos/mod/',(req,res)=>{
    const {id, titulo, descripcion} = req.body;
    const topic_mod = {
        id: id,
        titulo: titulo,
        descripcion: descripcion
    };
    let topic_id = topic_mod.id;
    topicos.forEach(topico => {
        if(topico.id = topic_id){
            topico.id = topic_mod.id;
            topico.titulo = topic_mod.titulo;
            topico.descripcion = topic_mod.descripcion;
        }
        return res.send(topico);
    });
    // console.log('Topico modificado exitosamente');
})

/**
 * @swagger
 * /topicos/del:
 *  delete:
 *    description: Elimina un topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 server.delete('/topicos/del/',(req,res)=>{
    const {id} = req.body;
    
    let user_id = req.body.id;
    topicos.forEach((topico,i) => {
        if(topico.id = user_id){
            topicos.splice(i,1)
            return res.send('Topico eliminado.');
        }
    });
})

/**
 * @swagger
 * /topicos:
 *  get:
 *    description: Devuelve la lista de los topicos
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.get('/topicos', (req,res)=>res.send(topicos));

//ENDPOINTS COMENTARIOS

/**
 * @swagger
 * /comentarios/alta:
 *  post:
 *    description: Crea un nuevo topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: topico_id
 *      description: ID del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: user_id
 *      description: ID del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: comentario
 *      description: Comentario del tópico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */
server.post('/comentarios/alta',(req,res)=>{
    const {id, topico_id, user_id, comentario} = req.body;
    const coment = {
        id: id,
        topico_id: topico_id,
        user_id: user_id,
        comentario: comentario
    };
    comentarios.push(coment);
    return res.send(coment);
    // console.log('Topico creado exitosamente');
})

/**
 * @swagger
 * /comentarios/mod:
 *  put:
 *    description: Modifica topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: topico_id
 *      description: ID del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: user_id
 *      description: ID del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: comentario
 *      description: Comentario del tópico
 *      in: formData
 *      required: true
 *      type: string
 * 
 */

server.put('/comentarios/mod/',(req,res)=>{
    const {id, topico_id, user_id, comentario} = req.body;
    const coment_mod = {
        id: id,
        topico_id: topico_id,
        user_id: user_id,
        comentario: comentario
    };
    let coment_id = coment_mod.id;
    comentarios.forEach(comentario => {
        if(comentario.id = coment_id){
            comentario.id = coment_mod.id;
            comentario.topico_id = coment_mod.topico_id;
            comentario.user_id = coment_mod.user_id;
            comentario.comentario = coment_mod.comentario;
        }
        return res.send(comentario);
    });
    // console.log('Topico modificado exitosamente');
})

/**
 * @swagger
 * /comentarios/del:
 *  delete:
 *    description: Elimina un comentario
 *    parameters:
 *    - name: id
 *      description: Id del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 server.delete('/comentarios/del/',(req,res)=>{
    const {id} = req.body;
    
    let coment_id = req.body.id;
    comentarios.forEach((comentario,i) => {
        if(comentario.id = coment_id){
            comentarios.splice(i,1)
            return res.send('Comentario eliminado.');
        }
    });
})

/**
 * @swagger
 * /comentarios:
 *  get:
 *    description: Devuelve la lista de los comentarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.get('/comentarios', (req,res)=>res.send(comentarios));


server.listen(3000, () => console.log('Corriendo en el puerto 3000'));
