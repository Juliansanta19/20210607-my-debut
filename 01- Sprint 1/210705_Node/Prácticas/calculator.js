const operaciones = require("../Librerias/Mis_funciones");
const archivos_generar = require("../Librerias/Crear_archivo");

var val_1 = 8;
var val_2 = 2;
let sumar = operaciones.sumar(val_1, val_2);
console.log(`${val_1} + ${val_2} = ${sumar}`);

archivos_generar.crear_archivo_plano("op.txt", `${val_1} + ${val_2} = ${sumar}`);

var val_1 = 10;
var val_2 = 5;
let restar = operaciones.restar(val_1, val_2);
console.log(`${val_1} - ${val_2} = ${restar}`);

archivos_generar.crear_archivo_plano("op.txt", `${val_1} - ${val_2} = ${restar}`);

var val_1 = 4;
var val_2 = 3;
let multiplicar = operaciones.multiplicar(val_1, val_2);
console.log(`${val_1} * ${val_2} = ${multiplicar}`);

archivos_generar.crear_archivo_plano("op.txt", `${val_1} * ${val_2} = ${multiplicar}`);

var val_1 = 25;
var val_2 = 5;
let dividir = operaciones.dividir(val_1, val_2);
console.log(`${val_1} / ${val_2} = ${dividir}`);

archivos_generar.crear_archivo_plano("op.txt", `${val_1} / ${val_2} = ${dividir}`);



