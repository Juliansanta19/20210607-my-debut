function sumar(val_1, val_2){
    if (isNaN(val_1) === false && isNaN(val_2) === false){
return val_1 + val_2
}else{
    console.log("Número inválido")
}
}
function restar(val_1, val_2){
    if (isNaN(val_1) === false && isNaN(val_2) === false){
return val_1 - val_2
}else{
    console.log("Número inválido")
}
}
function multiplicar(val_1, val_2){
    if (isNaN(val_1) === false && isNaN(val_2) === false){
return val_1 * val_2
}else{
    console.log("Número inválido")
}
}
function dividir(val_1, val_2){
    if (isNaN(val_1) === false && isNaN(val_2) === false){
return val_1 / val_2
}else{
    console.log("Número inválido")
}
}

var msg_ok = "Operación realizada exitosamente"
var msg_fail = "La operación no se pudo completar"

exports.sumar = sumar
exports.restar = restar
exports.multiplicar = multiplicar
exports.dividir = dividir
exports.msg_ok = msg_ok
exports.msg_fail = msg_fail
