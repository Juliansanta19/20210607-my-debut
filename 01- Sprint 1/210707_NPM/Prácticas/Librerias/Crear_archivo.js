const fs = require("fs");

//Función que permite crear archivos

//Params: file > el nombre del archivo, por ejemplo log.txt
//Params: text > el contenido del archivos, "hola, saludo, etc"

function crear_archivo_plano(file,text){
    fs.appendFile(file, text + "\n", function(err){
        if(err){
            return console.log(err);
        }
    } )
}
exports.crear_archivo_plano = crear_archivo_plano;