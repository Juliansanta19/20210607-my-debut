//Lo siguiente es para relacionar las variables de entorno con la App:

var env = require("./var_entorno.json");

//Traer en qué ambiente se está corriendo la App
var node_env = process.env.NODE_ENV || "development";

var variables = env[node_env];

console.log(`La App está corriendo en el puerto ${variables.PORT}`);

//
