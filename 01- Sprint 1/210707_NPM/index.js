const chalk = require("chalk");

console.log(chalk.red("Hello World"));


// Varialbles de entorno

var env = require("./env.environment.json");
//obtener el valor del enviroment configurado
console.log(env.development.SERVERURL);
//obtener el environment del servidor, configurar variable de entorno en la maquina
// var node_env = process.env.NODE_ENV;
// console.log('environment mi maquina',node_env) // dev
var node_env_local = process.env.NODE_ENV || 'development';
//sacar el valor del objeto
var variables = env[node_env_local];
console.log(`El puerto de desarrollo es: ${variables.PORT} paquete actualizado por el momento y su server url es ${variables.SERVERURL}`);


// Ejercicio fechas

var moment = require("moment");

//fecha actual

var now = moment(new Date());

//fecha utc

var utc = moment(new Date()).utc();

console.log("actual", now);
console.log("utc", utc);
 var diferencia = now.hours() - utc.hours;
 console.log("diferencia", diferencia);

 //muestra ambas fechas

 console.log(now.format("DD MM YYYY"));
 console.log(utc.format("hh:mm:ss"));

 var duration = new moment(utc.diff(now));

 var primera_fecha = moment([2021, 07, 07]);
 var segunda_fecha = moment([2021, 07, 12]);

// Diferncias en dias
var result_dias_diff = primera_fecha.diff(segunda_fecha, 'days');
console.log("Nro dias diff:", result_dias_diff);
// Diferncias en horas
var result_horas_diff = primera_fecha.diff(segunda_fecha, 'hours');
console.log("Nro horas diff:", result_horas_diff);
//valor absoluto
var duration = moment.duration(primera_fecha.diff(segunda_fecha));
var days = Math.abs((duration.asDays()));
console.log(days);
//Identificar que fecha es mayor o menor utilizando el metodo isBefore
var fecha1 = '1980-06-10';
var fecha2 = '2021-07-07';
// ¿la fecha1 esta antes de la fecha 2 ? >>> True
console.log(moment(fecha1).isBefore(fecha2));
if(moment(fecha1).isBefore(fecha2)){
    console.log('La fecha2 '+fecha2+' es mayor que la fecha1 '+fecha1);  
}else{
    console.log('La fecha1 '+fecha1+' es mayor que la fecha2 '+fecha2);  
}

// npm i moment
var moment = require('moment');
//fecha actual
var now = moment(new Date());
//fecha utc
var utc = moment(new Date()).utc();
console.log('actual',now);
console.log('utc',utc);
var diferencia = now.hours() - utc.hours();
console.log('diferencia',diferencia);
// //Muestro ambas fechas
console.log(now.format('DD MM YYYY'));
console.log(utc.format('hh:mm:ss'));
var duration = new moment(utc.diff(now));
var primera_fecha = moment([2021, 07, 07]);
var segunda_fecha = moment([2021, 07, 12]);
// Diferncias en dias
var result_dias_diff = primera_fecha.diff(segunda_fecha, 'days');
console.log("Nro dias diff:", result_dias_diff);
// Diferncias en horas
var result_horas_diff = primera_fecha.diff(segunda_fecha, 'hours');
console.log("Nro horas diff:", result_horas_diff);
//valor absoluto
var duration = moment.duration(primera_fecha.diff(segunda_fecha));
var days = Math.abs((duration.asDays()));
console.log(days);
//Identificar que fecha es mayor o menor utilizando el metodo isBefore
var fecha1 = '1980-06-10';
var fecha2 = '2021-07-07';
// ¿la fecha1 esta antes de la fecha 2 ? >>> True
console.log(moment(fecha1).isBefore(fecha2));
if(moment(fecha1).isBefore(fecha2)){
    console.log('La fecha2 '+fecha2+' es mayor que la fecha1 '+fecha1);  
}else{
    console.log('La fecha1 '+fecha1+' es mayor que la fecha2 '+fecha2);  
}
