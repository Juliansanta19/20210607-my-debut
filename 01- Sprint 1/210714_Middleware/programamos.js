const compression = require("compression")
const express = require('express')
const app = express();


app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//middleware global, genero un log con cada ejecución
app.use(function (req, res, next) {
    console.log(req.url);
    next();
});

const users = [
    {id:1, nombre: "Pepe", email: "pepe@nada.com" },
    {id:2, nombre: "Hugo", email: "hugo@nada.com"} ,
    {id:3, nombre: "Juan", email: "juan@nada.com"},
    {id:4, nombre: "Maria", email: "maria@nada.com"}
];  

app.get('/users', function (req, res){
    res.json(users);
});

app.get('/users/:id', function (req, res) {
    let user_id = parseInt(req.params.id)
    let indice = false
    let mi_respuesta = {user_id: user_id};//object literal

    /* mostrar diferentes maneras de buscar */

    /* opcion 1 */
    
    
    // users.forEach(function (value, index){
    //     console.log("value id: " + value.id)
    //     console.log("user_id: " + user_id)
    //     if(value.id == user_id){
    //         indice = index
    //     }
    // })

    // if(indice === false){
    //     mi_respuesta.msj = "Usuario no encontrado"
    // }else{
    //     mi_respuesta.msj = "Usuario encontrado"
    //     mi_respuesta.user = users[indice]
    // }    

    /* opcion 2 */

    const result = users.filter(user => user.id == user_id);
    console.log(result[0])
    if(result[0] === undefined){
        mi_respuesta.msj = "Usuario no encontrado"
    }else{
        mi_respuesta.msj = "Usuario encontrado"
        mi_respuesta.user = result[0]
    }

    res.json(mi_respuesta)
    
});

app.post('/users_by_name', function (req, res) {

    let name = req.body.name
    let mi_respuesta = {search: name}

    const result = users.filter(user => user.nombre.toLowerCase() === name.toLowerCase());


    if(result.length == 0){
        mi_respuesta.msj = "Usuario no encontrado"
    }else{
        mi_respuesta.msj = "Usuario encontrado"
        mi_respuesta.users = result
    }

    res.json(mi_respuesta)
    
});



 
app.listen(3000, function() {
    console.log(`Express corriendo en http://localhost:3000`)
})
