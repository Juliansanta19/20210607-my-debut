const compression = require("compression")
const express = require('express');
const server = express();
const comp = compression();

server.get('/', function (req, res) {
  res.send('Hola mundo');
});

server.use(function(req, res, next){
  console.log(req.url);
  next();
})

function validarUsuario(req, res, next){
  if(req.query.usuario != "admin"){
    res.json("Usuario inválido");
  }else{
    next()
  }

}

function interceptar(req, res, next){
  res.json("Acceso denegado")
}


server.use(function(req, res, next){
  console.log("Time:", Date.now())
})

server.listen(3000, function () {
  console.log('Escuchando el puerto 3000!');
});


