//Otro ejemplo

server.get('/barato',(req,res) => {
    let menor = 0
    let menor_precio = parseInt(telefonos[0].precio)
    telefonos.forEach((telefono,i) => {
        let precio_telefono = parseInt(telefono.precio)
        if ( precio_telefono < menor_precio ) {
            menor_precio = precio_telefono
            menor = i
        } 
    })
    res.send(telefonos[menor])
})


// Ejemplo Luis
let masBajo = telefonos.reduce(
    (min, tel) => (parseInt(tel.precio) < min ? parseInt(tel.precio) : min),
    parseInt(telefonos[0].precio)
  );
  res.send(JSON.stringify(masBajo));
});
app.get("/masalto", function (req, res) {
  let masalto = telefonos.reduce(
    (may, tel) => (parseInt(tel.precio) > may ? parseInt(tel.precio) : may),
    parseInt(telefonos[0].precio)
  );
  res.send(JSON.stringify(masalto));
});
app.get("/agrupar", function (req, res) { 
  const agrupar = (telefonos, propiedad_agrupar) => {
    return telefonos.reduce(function (total, obj) {
      let propiedad_key = obj[propiedad_agrupar];
      //si esta propiedad no existe en el objeto total
      if (!total[propiedad_key]) {
        //crea un nuevo arreglo
        total[propiedad_key] = [];
      }
      //agrega un valor a cada array segun la propiedad que pertenezca
      total[propiedad_key].push(obj);
      return total;
    }, {});
  };