var express = require("express");
var app = express()

app.get("/saludo", function (req, res) {
    res.send("Hola Mundo! Estoy saludando");
});



Familia = [
    {nombre: "Fernando",
    apellido: "Gracía",
    edad: 33,
    parent: "novio"},

    {nombre: "Ana María",
    apellido: "Berdomás",
    edad: 54,
    parent: "madre"},

    {nombre: "Nora",
    apellido: "Argibay",
    edad: 81,
    parent: "abuela"},

    {nombre: "Fernando",
    apellido: "Santamaria",
    edad: 31,
    parent: "hermano"},

    {nombre: "Melina",
    apellido: "Mira",
    edad: 24,
    parent: "cuñada"},

];



app.get("/familia", function (req, res) {
  
    res.send(`Hola! Esta es mi familia: ${Familia}`);
});


app.listen(3000, function () {
    console.log("Escuchando el puerto 3000");
});
