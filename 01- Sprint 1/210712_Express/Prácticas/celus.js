//const { json } = require("express");
const express = require('express');
const app = express();

let telefonos = [{
    marca: "Samsung",
    modelo: "S10",
    gama: "Alta",
    pantalla: "19:9",
    sistema_operativo: "Android",
    precio: "1000"
},{
    marca: "Iphone",
    modelo: "12 Pro",
    gama: "Alta",
    pantalla: "OLED",
    sistema_operativo: "iOS",
    precio: "1200"
},{
    marca: "Nokia",
    modelo: "1100",
    gama: "Baja",
    pantalla: "LCD",
    sistema_operativo: "Series 40",
    precio: "150"
},{
    marca: "Xiaomi",
    modelo: "Mi 9T Pro",
    gama: "Media",
    pantalla: "LCD",
    sistema_operativo: "Android 9 Pie",
    precio: "450"
}]

app.get('/', function (req, res) {
    res.json(telefonos)
  })
  
  app.get('/medio', function (req, res) {
    res.json(telefonos.splice(0,(telefonos.length/2)))
})


app.get("/masbajo", function (req, res) {
    let masBajo = telefonos.reduce(
      (min, tel) => (parseInt(tel.precio) < min ? parseInt(tel.precio) : min),
      parseInt(telefonos[0].precio)
    );
    res.send(JSON.stringify(masBajo));
  });
  
  app.get("/masalto", function (req, res) {
    let masalto = telefonos.reduce(
      (may, tel) => (parseInt(tel.precio) > may ? parseInt(tel.precio) : may),
      parseInt(telefonos[0].precio)
    );
    res.send(JSON.stringify(masalto));
  });
  
  app.get("/agrupar", function (req, res) { 
  
    const agrupar = (telefonos, propiedad_agrupar) => {
      return telefonos.reduce(function (total, obj) {
        let propiedad_key = obj[propiedad_agrupar];
        //si esta propiedad no existe en el objeto total
        if (!total[propiedad_key]) {
          //crea un nuevo arreglo
          total[propiedad_key] = [];
        }
        //agrega un valor a cada array segun la propiedad que pertenezca
        total[propiedad_key].push(obj);
        return total;
      }, {});
    };
  
    const resultado_agrupar = agrupar(telefonos, "gama");
  
    res.send(resultado_agrupar);
  });
  


 
app.listen(3000, function() {
    console.log(`Express corriendo en http://localhost:3000`)
})
