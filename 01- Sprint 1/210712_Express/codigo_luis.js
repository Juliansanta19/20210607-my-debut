const express = require("express");
const app = express();
//ubicamos el puerto del servidor con las variables de entorno
const env = require("./variables.entorno.json");
const node_env = process.env.NODE_ENV || "development";
const puerto = env[node_env];
//aceptar valores json
//Este método se llama como middleware
app.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
app.use(express.urlencoded({ extended: true }));
//configuramos los endpoints con los verbos HTTP : GET, POST, PUT, DELETE
//ESTRUCTURA
//
//app.nombre_verbo('ruta_del_endpoint', callback_funcion);
//ejm.ruta_del_endpoint('/usuarios')
//ejm.ruta_del_endpoint('/carros')
//ejem.callback_funcion => function(req, res)
/*
GET = Ob                er
POST = Crear
PUT = Actualizar
DELETE = Eliminar
*/
//array para almecenar datos
let usuario = {
  nombre: "",
  apellido: "",
};
let respuesta = {
  error: "",
  codigo: "",
  mensaje: "",
};
let datos = [];
//get >obtener informacion
app.get("/", function (req, res) {
  console.log("hola mundo:");
  res.json("Hola Mundo");
});
//get >obtener informacion
//nombre del recurso: saludo
app.get("/saludo", function (req, res) {  
  res.json("Hola sin query params");
});
//get >obtener informacion
///saludo/luis
app.get("/saludo/:nombre", function (req, res) {  
  res.json("Hola bienvenido " + req.params.nombre);
});
//codigos status:200 or 201 son OK
app.get("saludo/status/", function (req, res) {
  res.json("Saludos ok");
});
//post >creacion
app.post("/saludo/", function (req, res) {
  res.json("Saludos desde el método POST");
});
//PUT >actualizar
app.put("/saludo/", function (req, res) {
  res.json("Saludos desde el método PUT");
});
//DELETE >borrar
app.delete("/saludo/", function (req, res) {
  res.json("Saludos desde el método DELETE");
});
//USUARIOS
//CREACION DE USUARIOS
app.post("/usuario", function (req, res) {
  console.table(req.body);
  if (!req.body.nombre || !req.body.apellido) {
    respuesta = {
      error: true,
      codigo: 502,
      mensaje: "El campo nombre y apellido son requeridos",
    };
  } else {
    if (req.body.nombre !== "" || req.body.apellido !== "") {
      usuario = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
      };
      respuesta = {
        error: false,
        codigo: 200,
        mensaje: "Usuario creado",
        respuesta: usuario,
      };
      datos.push(usuario);
      console.log(datos);
    }
  }
  res.send(respuesta);
});
app.delete("/usuario", function (req, res) {
  console.table(req.body);
  if (!req.body.nombre) {
    respuesta = {
      error: true,
      codigo: 502,
      mensaje: "El campo nombre es requerido",
    };
  } else {
    if (req.body.nombre !== "") {
      let nombre_data = req.body.nombre;
      let filter_data = datos.filter((x) => x.nombre !== nombre_data);
      datos = [...filter_data];
      console.log(datos);
    }
    respuesta = {
      error: false,
      codigo: 200,
      mensaje: "Usuario borrado",
      respuesta: usuario,
    };
  }
  res.send(respuesta);
});
//actualizar
app.put("/usuario", function (req, res) {
  if (!req.body.nombre || !req.body.apellido) {
    respuesta = {
      error: true,
      codigo: 502,
      mensaje: "El campo nombre y apellido son requeridos",
    };
  } else {
    if (req.body.nombre !== "" || req.body.nombre !== "") {
      let nombre_req = req.body.nombre;
      let apellido_req = req.body.apellido;
      datos.map((item) => {
        if (item.nombre === nombre_req) {
          item.apellido = apellido_req;
        }
      });
      respuesta = {
        error: false,
        codigo: 200,
        mensaje: "Usuario actualizado",
        respuesta: usuario,
      };
    }
  }
  res.send(respuesta);
});
app.get("/usuarios", function (req, res) {
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: "Lista de Usuarios",
    usuarios: datos,
  };
  res.send(respuesta);
});
//configuramos expreess para realizar el llamado
//todas las peticiones sobre mi servidor van a recibirse sobre este puerto 3000
app.listen(puerto, function () {
  console.log("Server corriendo sobre el puerto " + puerto.port);
});

let telefonos = [{
    marca: "Samsung",
    modelo: "S10",
    gama: "Alta",
    pantalla: "19:9",
    sistema_operativo: "Android",
    precio: "1000"
},{
    marca: "Iphone",
    modelo: "12 Pro",
    gama: "Alta",
    pantalla: "OLED",
    sistema_operativo: "iOS",
    precio: "1200"
},{
    marca: "Nokia",
    modelo: "1100",
    gama: "Baja",
    pantalla: "LCD",
    sistema_operativo: "Series 40",
    precio: "150"
},{
    marca: "Xiaomi",
    modelo: "Mi 9T Pro",
    gama: "Media",
    pantalla: "LCD",
    sistema_operativo: "Android 9 Pie",
    precio: "450"
}]

app.get("/", function (req, res){
    res.json(telefonos);
})
app.get("/medio", function (req, res){
    res.json(telefonos.splice(0,(telefonos.length/2)));
})
app.listen(3000, function (){
    console.log("Express corriendo en http://localhost:3000");
})