const express = require('express');
const app = express();

app.get('/', function (req, res) {
  res.send('Hola mundo');
});

app.listen(3000, function () {
  console.log('Escuchando el puerto 3000!');
});

// app.get('/holis', function (req, res) {
//     res.send('Esta es la prueba N°2');
//   });