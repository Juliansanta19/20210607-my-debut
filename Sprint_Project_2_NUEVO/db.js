const {Sequelize} = require("sequelize");

const PeliculaModelo = require("./models/pelicula");

const sequelize = new Sequelize("Sprint_Project_2_Prueba", "root", "", {
    host: "localhost",
    dialect: "mysql",
});

// validar_conexion()


const Pelicula = PeliculaModelo(sequelize, Sequelize);

sequelize.sync({force: false}).then(() => {
    console.log("Tablas Sincronizadas");
});


async function validar_conexion() {
    try {
        await sequelize.authenticate();
        console.log("Conexion ha sido establecida correctamente");
    } catch (error){
        console.error("Error al conectarse a la base de datos", error);
    }
};


module.exports = {
    Pelicula,
}
