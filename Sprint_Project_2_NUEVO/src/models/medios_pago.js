module.exports = (sequelize, type) => {

    return sequelize.define(
        "PayMethod",
        {
            idPayMethod: {
                type: type.INTEGER,
                primaryKey: true,
                autoIncremet: true
            },
            nombre: type.STRING,
        },{
            sequelize,
            timestamps: false, //para que no se incluyan en la DB dos carpetas automáticas
        }
    );
};