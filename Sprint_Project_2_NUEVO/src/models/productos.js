module.exports = (sequelize, type) => {

    return sequelize.define(
        "Products",
        {
            idProduct: {
                type: type.INTEGER,
                primaryKey: true,
                autoIncremet: true
            },
            nombre: type.STRING,
            precio: type.INTEGER,
        },{
            sequelize,
            timestamps: false, //para que no se incluyan en la DB dos carpetas automáticas
        }
    );
};