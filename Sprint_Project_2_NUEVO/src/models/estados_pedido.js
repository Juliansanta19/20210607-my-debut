module.exports = (sequelize, type) => {

    return sequelize.define(
        "OrderStatus",
        {
            idStatus: {
                type: type.INTEGER,
                primaryKey: true,
                autoIncremet: true
            },
            nombreStatus: type.STRING,
        },{
            sequelize,
            timestamps: false, //para que no se incluyan en la DB dos carpetas automáticas
        }
    );
};