module.exports = (sequelize, type) => {

    return sequelize.define(
        "Orders",
        {
            idOrder: {
                type: type.INTEGER,
                primaryKey: true,
                autoIncremet: true
            },
            carrito: type.INTEGER,
            totalPedido: type.INTEGER,
            formPago: type.INTEGER,
            horaPedido: type.STRING,
            estadoPedido: type.INTEGER,
            idUser: type.INTEGER,
            usuario: type.STRING,
            nombreComp: type.STRING,
            domicilio: type.STRING,
            telefono: type.STRING,
            correo: type.STRING,
        },{
            sequelize,
            timestamps: false, //para que no se incluyan en la DB dos carpetas automáticas
        }
    );
};