module.exports = (sequelize, type) => {

    return sequelize.define(
        "Users",
        {
            idUser: {
                type: type.INTEGER,
                primaryKey: true,
                autoIncremet: true
            },
            usuario: type.STRING,
            nombreComp: type.STRING,
            correo: type.STRING,
            telefono: type.STRING,
            domicilio: type.STRING,
            pass: type.STRING,
            admin: type.BOOLEAN,
            logueado: type.BOOLEAN,
            carrito: type.INTEGER,
        },{
            sequelize,
            timestamps: false, //para que no se incluyan en la DB dos carpetas automáticas
        }
    );
};