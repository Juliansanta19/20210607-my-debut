const {PayMethod} = require("../database/sync");

exports.index = (async (req, res) =>{
    const payMethod = await PayMethod.findAll();

    res.json({
        mensaje: "OK",
        payMethod_data: payMethod,
    });
});

