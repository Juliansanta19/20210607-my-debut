const {Products} = require("../database/sync");

exports.index = (async (req, res) =>{
    const products = await Products.findAll();

    res.json({
        mensaje: "OK",
        products_data: products,
    });
});

