const {OrderStatus} = require("../database/sync");

exports.index = (async (req, res) =>{
    const orderStatus = await OrderStatus.findAll();

    res.json({
        mensaje: "OK",
        orderStatus_data: orderStatus,
    });
});

