const {Users} = require("../database/sync");

exports.index = (async (req, res) =>{
    const users = await Users.findAll();

    res.json({
        mensaje: "OK",
        users_data: users,
    });
});

