const {Orders} = require("../database/sync");

exports.index = (async (req, res) =>{
    const orders = await Orders.findAll();

    res.json({
        mensaje: "OK",
        orders_data: orders,
    });
});

