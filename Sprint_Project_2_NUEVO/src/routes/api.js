const router = require("express").Router();

const apiUsers = require("./api/usuarios");
const apiProducts = require("./api/productos");
const apiOrders = require("./api/pedidos");
const apiPayMethod = require("./api/medios_pago");

router.use("./usuarios", apiUsers);
router.use("./productos", apiProducts);
router.use("./pedidos", apiOrders);
router.use("./medios_pago", apiPayMethod);


module.exports = router