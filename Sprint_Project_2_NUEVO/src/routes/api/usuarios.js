const router = require("express").Router();
const {Users} = require("../../../database/sync");
const controller = require("../../controllers/usuarios");

//Listado de usuarios

router.get("/", async (req, res) => {

    const users = await Users.findAll();

    res.json(users)
});

//----------------------------------------------

/**
 * @swagger
 * /usuarios/registro:
 *  post:
 *    description: Registra un usuario nuevo
 *    parameters:
 *    - name: usuario
 *      description: Usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: nombreComp
 *      description: Nombre Completo
 *      in: formData
 *      required: false
 *      type: string
 *    - name: correo
 *      description: Correo Electrónico
 *      in: formData
 *      required: false
 *      type: string
 *    - name: telefono
 *      description: Teléfono
 *      in: formData
 *      required: false
 *      type: string
 *    - name: domicilio
 *      description: Domicilio
 *      in: formData
 *      required: false
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */

 router.post("/registro", (req, res) =>{
    let {usuario, nombreComp, correo, telefono, domicilio, pass} = req.body;
    const one_user = {
        idUser: users[users.length -1].idUser + 1,
        usuario: usuario,
        nombreComp: nombreComp,
        correo: correo,
        telefono: telefono,
        domicilio: domicilio,
        pass: pass,
        admin: false,
        logueado: false,
        carrito: []
    };

    if (nombreComp === "" || nombreComp === null) {
        return res.json("El nombre es inválido");
    }
        for(let i=0; i < users.length; i++) {
 
        let itemUsers = users[i]
 
    if(correo === "" || correo === null || itemUsers.correo === correo){
        return res.json("El correo es inválido o ya está en uso");
        }
    if (usuario === "" || usuario === null || itemUsers.usuario === usuario) {
        return res.json("El usuario es inválido o ya está en uso")
    }
}

    if (telefono === "" || telefono === null) {
        return res.json("El teléfono es inválido");
    }

    if (domicilio === "" || domicilio === null) {
        return res.json("El domicilio es inválido");
    }

    if (pass === "" || pass === null) {
        return res.json("La contraseña es inválida");
    }else{
        users.push(one_user);
        console.log(one_user)
        res.json("USUARIO CREADO CORRECTAMENTE");
    }  
});



/**
 * @swagger
 * /usuarios/login:
 *  post:
 *    description: Login de usuario
 *    parameters:
 *    - name: usuario
 *      description: Usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */


router.post('/login', (req,res) =>{
    const {usuario, pass} = req.body;
    console.log(usuario,pass);
    let index = -1;
    users.forEach((user , i) => {
        if(user.usuario == usuario){
           if(user.pass == pass){
            index = i;
          }
        }
     });
    if(index == -1){
        res.json("Usuario o contraseña inválido");
    } else{
        users[index].logueado = true;
        console.log(users[index]);
        res.json(`Bienvenido ${users[index].nombreComp}`);
    }
})



//----------------------------------------------



module.exports = router