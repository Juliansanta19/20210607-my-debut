const router = require("express").Router();
const {PayMethod} = require("../../../database/sync");
//const controller = require("../../../controllers/frutas");

//Listado de usuarios

router.get("/", async (req, res) => {

    const payMethod = await PayMethod.findAll();

    res.json(payMethod)
});

//----------------------------------------------

/**
 * @swagger
 * /medios_pago/{idUser}:
 *  get:
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    description: Ver Todos los Medios de Pago Habilitados
 *    responses:
 *      200:
 *        description: Success * 
 */

 router.get('/:id', userLogueado, userAdmin,(req,res)=>{
    res.json(payMethod);
});


/**
 * @swagger
 * /medios_pago/alta_medios/{idUser}:
 *  post:
 *    description: Alta de Medios de Pago (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nombre del Nuevo Medio de Pago
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */

router.post('/alta_medios/:id', userLogueado, userAdmin, (req,res)=>{

    const one_payMethod = {
            idPayMethod: payMethod[payMethod.length -1].idPayMethod + 1,
            nombre: (req.body.nombre).toUpperCase(),
    };

    if (!req.body.nombre) {
        
        res.json("DEBE COMPLETAR EL NOMBRE DEL NUEVO MEDIO DE PAGO");

    }else{

    payMethod.push(one_payMethod);
    console.log(payMethod);
    res.json(payMethod);

    };

});


/**
 * @swagger
 * /medios_pago/modificar_medios/{idUser}:
 *  put:
 *    description: Modificación de Medios de Pago (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idPayMethod
 *      description: ID del Medio de Pago
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nuevo Nombre del Medio de Pago
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success * 
 */

router.put('/modificar_medios/:id', userLogueado, userAdmin, (req,res)=>{

    let i = payMethod.findIndex(method => method.idPayMethod == parseInt(req.body.idPayMethod));

    if(i != -1){
            
        if (!req.body.idPayMethod) {
        
            res.json("DEBE COMPLETAR EL ID DEL MEDIO DE PAGO");
        
    }else{
        
        payMethod[i].nombre = (req.body.nombre).toUpperCase();

        res.json("MEDIO DE PAGO MODIFICADO");

        console.log(payMethod);

    };

        }else{

            res.json("NO SE HA ENCONTRADO EL MEDIO DE PAGO");
        };
    });


/**
 * @swagger
 * /medios_pago/eliminar_medios/{idUser}:
 *  delete:
 *    description: Eliminar Medio de Pago (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idPayMethod
 *      description: ID del Medio de Pago
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

router.delete('/eliminar_medios/:id', userLogueado, userAdmin, (req,res)=>{

    let i = payMethod.findIndex(method => method.idPayMethod === parseInt(req.body.idPayMethod));

    if(i != -1){

        if (!req.body.idPayMethod) {
        
            res.json("DEBE COMPLETAR TODOS LOS CAMPOS");
    
        }else{
    
        payMethod.splice(i, 1);

        res.json("MEDIO DE PAGO ELIMINADO");
        console.log(payMethod);

        };

    }else{

        res.json("MEDIO DE PAGO NO ENCONTRADO");
    };
});




//----------------------------------------------



module.exports = router