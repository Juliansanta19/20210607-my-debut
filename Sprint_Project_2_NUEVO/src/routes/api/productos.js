const router = require("express").Router();
const {Products} = require("../../../database/sync");
//const controller = require("../../../controllers/frutas");

//Listado de usuarios

router.get("/", async (req, res) => {

    const products = await Products.findAll();

    res.json(products)
});

//----------------------------------------------

/**
 * @swagger
 * /productos/{idUser}:
 *  get:
 *    description: Listado de productos
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

 router.get('/:id', userLogueado, (req,res)=>{
    res.json(products);
});


/**
 * @swagger
 * /productos/alta_productos/{idUser}:
 *  post:
 *    description: Alta de Productos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nombre del Producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: precio
 *      description: Precio del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */


 router.post('/alta_productos/:id', userLogueado, userAdmin, (req,res)=>{
        let {nombre, precio} = req.body;
            const one_product = {
                idProduct: products[products.length -1].idProduct + 1,
                nombre: nombre,
                precio: parseInt(precio),
};

    if (!req.body.nombre || !req.body.precio) {
        
        res.json("DEBE COMPLETAR TODOS LOS CAMPOS");

    }else{

        products.push(one_product);
        console.log(products);
        res.json("PRODUCTO DADO DE ALTA CORRECTAMENTE");

    }

});


/**
 * @swagger
 * /productos/modif_productos/{idUser}:
 *  put:
 *    description: Modificación de Productos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: nombre
 *      description: Nuevo Nombre del Producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: precio
 *      description: Nuevo Precio del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */


router.put('/modif_productos/:id', userLogueado, userAdmin,(req,res)=>{

        let i = products.findIndex(product => product.idProduct == parseInt(req.body.idProduct));

        if(i != -1){
            products[i].nombre = req.body.nombre;
            products[i].precio = parseInt(req.body.precio);

            if (!req.body.nombre || !req.body.precio) {
        
                res.json("DEBE COMPLETAR TODOS LOS CAMPOS");

        }else{
            res.json(products[i]);
            console.log(products[i]);

        }

        }else{
            res.json("NO SE HA ENCONTRADO EL PRODUCTO");
        }
    });


/**
 * @swagger
 * /productos/elim_productos/{idUser}:
 *  delete:
 *    description: Eliminar Productos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

router.delete('/elim_productos/:id', userLogueado, userAdmin, (req,res)=>{

    let i = products.findIndex(product => product.idProduct === parseInt(req.body.idProduct));

    if(i != -1){
        if (!req.body.idProduct) {
        
            res.json("DEBE COMPLETAR EL ID A ELIMINAR");
    
        }else{

        products.splice(i, 1);
        res.json("PRODUCTO ELIMINADO");
        console.log(products);

        }

    }else{

        res.json("NO SE HA ENCONTRADO EL PRODUCTO");
    }

});




//----------------------------------------------



module.exports = router