const router = require("express").Router();
const {Orders} = require("../../../database/sync");
//const controller = require("../../../controllers/frutas");

//Listado de usuarios

router.get("/", async (req, res) => {

    const orders = await Orders.findAll();

    res.json(orders)
});

//----------------------------------------------

/**
 * @swagger
 * /pedidos/carrito/agregar/{idUser}:
 *  post:
 *    description: Agregar Productos al Carrito
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: cant
 *      description: Cantidad del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

 router.post('/carrito/agregar/:id', userLogueado, (req,res)=>{

    let i = products.findIndex(product => product.idProduct == parseInt(req.body.idProduct));
    let u = users.findIndex(user => user.idUser == parseInt(req.params.id))

    if(i != -1){
    const itemCarrito = {
        idProduct: parseInt(req.body.idProduct),
        nombreProd: products[i].nombre,
        precio: products[i].precio * req.body.cant,
        cant: parseInt(req.body.cant),
    }

    if (!req.body.idProduct || !req.body.cant) {
        
        res.json("DEBE COMPLETAR TODOS LOS CAMPOS");

    }else{

    users[u].carrito.push(itemCarrito);
    res.json(users[u].carrito); //CARRITO ACTUALIZADO

    }

    }else{

        res.json("PRODUCTO NO DISPONIBLE");

    };

 });


/**
 * @swagger
 * /pedidos/carrito/eliminar/{idUser}:
 *  delete:
 *    description: Eliminar Productos del Carrito
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idProduct
 *      description: ID del Producto
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

 router.delete('/carrito/eliminar/:id', userLogueado, (req,res)=>{

    let u = users.findIndex(user => user.idUser == parseInt(req.params.id));

    let i = users[u].carrito.findIndex(product => product.idProduct === parseInt(req.body.idProduct));


    if(i != -1){
        if (!req.body.idProduct) {
        
            res.json("DEBE COMPLETAR TODOS LOS CAMPOS");
    
        }else{
    
        users[u].carrito.splice(i, 1);
        res.json(users[u].carrito);

        };

    }else{

        res.json("NO SE HA ENCONTRADO EL PRODUCTO EN SU CARRITO");
    }
});


/**
 * @swagger
 * /pedidos/hacer_pedido/{idUser}:
 *  post:
 *    description: Hacer un Pedido
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: formPago
 *      description: Indicar Forma de Pago (Índice)
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

router.post('/hacer_pedido/:id', userLogueado, (req,res)=>{

    let u = users.findIndex(user => user.idUser == parseInt(req.params.id));

    let totalPedido = 0

    users[u].carrito.forEach(product => totalPedido += parseInt(product.precio));
    
    if(users[u].carrito.length != 0){

    const one_order = {
        idOrder: orders[orders.length -1].idOrder + 1,
        carrito: users[u].carrito,
        totalPedido: totalPedido,
        formPago: payMethod[req.body.formPago].nombre,
        horaPedido: now.format('MMMM Do YYYY, h:mm:ss a'),
        estadoPedido: orderStatus[0].nombreStatus,
        idUser: parseInt(req.params.id),
        usuario: users[u].usuario,
        nombreComp: users[u].nombreComp,
        domicilio: users[u].domicilio,
        telefono: users[u].telefono,
        correo: users[u].correo
    };
        orders.push(one_order);
        users[u].carrito = [];
        res.json(one_order); //SU PEDIDO SE ESTÁ PREPARANDO

    }else{

        res.json("SU CARRITO ESTÁ VACÍO"); 

    };
  
  });


/**
 * @swagger
 * /pedidos/mis_pedidos/{idUser}:
 *  get:
 *    description: Pedidos realizados por el Usuario
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

  router.get('/mis_pedidos/:id', userLogueado, (req,res)=>{

    let mis_pedidos = orders.filter(order => order.idUser == parseInt(req.params.id));

    if(mis_pedidos){

        res.json(mis_pedidos);

    }else{
        res.json("USTED AÚN NO HA REALIZADO NINGÚN PEDIDO");

    };
});


/**
 * @swagger
 * /pedidos/historial_pedidos/{idUser}:
 *  get:
 *    description: Todos los Pedidos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

router.get('/historial_pedidos/:id', userLogueado, userAdmin, (req,res)=>{

    res.json(orders);


});


/**
 * @swagger
 * /pedidos/actualizar_estado/{idUser}:
 *  put:
 *    description: Actualizar Estado de los Pedidos (Sólo Admin)
 *    parameters:
 *    - name: idUser
 *      description: ID del Usuario
 *      in: path
 *      required: false
 *      type: interger
 *    - name: idOrder
 *      description: ID del Pedido
 *      in: formData
 *      required: false
 *      type: interger
 *    - name: estadoPedido
 *      description: Nuevo Estado del Pedido (Índice)
 *      in: formData
 *      required: false
 *      type: interger
 *    responses:
 *      200:
 *        description: Success * 
 */

router.put('/actualizar_estado/:id', userLogueado, userAdmin,(req,res)=>{

    let i = orders.findIndex(order => order.idOrder == parseInt(req.body.idOrder));

    if(i != -1){

        if (!req.body.idOrder) {
        
            res.json("DEBE COMPLETAR EL ID DEL PEDIDO");
    
        }else{
    
            orders[i].estadoPedido = orderStatus[req.body.estadoPedido].nombreStatus; 
            // 0= Pendiente - 1= Confirmado - 2= En Preparación - 3= Enviado - 4= Entregado 
            res.json(`Estado de Producto modificado a: ${orderStatus[req.body.estadoPedido].nombreStatus}`);
            console.log(orders);

        };

    }else{

        res.json("NO SE HA ENCONTRADO LA ORDEN");
    }
});





//----------------------------------------------



module.exports = router