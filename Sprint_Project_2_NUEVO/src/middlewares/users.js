const userLogueado = (req, res , next) =>{
    let id_user = req.params.id;
    let flag = false;
    users.forEach(user => {
        if(user.idUser == id_user){
            if(user.logueado == true){
                flag = true;
            };
        };
    });
    if(flag == false){
    res.json("USTED NO ESTÁ LOGUEADO");
    }else{
        next();
    };
};


const userAdmin = (req, res, next) =>{
    let id_user = req.params.id;
    let flag = false;
    users.forEach(user => {
        if(user.idUser == id_user){
            if(user.admin == true){
                flag = true;
            };
        };
    });
    if(flag == false){
    res.json("USTED NO ES ADMINISTRADOR");
    }else{
        next();
    };
};


module.exports = {
    userLogueado,
    userAdmin
};