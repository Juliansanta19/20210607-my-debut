const {Sequelize} = require("sequelize");
const {dbDelilahFood} = require("./db");

const UsersModelo = require("../models/usuarios");
const ProductsModelo = require("../models/productos");
const OrdersModelo = require("../models/pedidos");
const PayMethodModelo = require("../models/medios_pago");
const OrderStatusModelo = require("../models/estados_pedido");

const Users = UsersModelo(dbDelilahFood, Sequelize);
const Products = ProductsModelo(dbDelilahFood, Sequelize);
const Orders = OrdersModelo(dbDelilahFood, Sequelize);
const PayMethod = PayMethodModelo(dbDelilahFood, Sequelize);
const OrderStatus = OrderStatusModelo(dbDelilahFood, Sequelize);

dbDelilahFood.sync({force: false}).then(() => {
    console.log("Tablas sincronizadas");
});

module.exports = {
    Users,
    Products,
    Orders,
    PayMethod,
    OrderStatus
}