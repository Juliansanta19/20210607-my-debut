module.exports = (sequelize, type) => {

    return sequelize.define("peliculas",{
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        titulo: type.STRING,
        descripcion: type.STRING,
        puntaje: type.INTEGER,
        director: type.STRING
        
    })
}