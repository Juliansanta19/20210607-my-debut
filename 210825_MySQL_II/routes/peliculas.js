const router = require('express').Router();
const {Pelicula} = require('../../db');
router.get('/hola', (req, res) => {   
    res.send('hola peliculas');
});
//listado de peliculas  /api/peliculas/
router.get('/', async (req, res) => {
    const peliculas = await Pelicula.findAll();
    res.json(peliculas);
});
// crear peliculas
router.post('/',async (req, res) => {
    const pelicula = await Pelicula.create(req.body);
    res.json(pelicula);
});
//actualizar la pelicula 
router.put('/:peliculaId',async (req, res) => {
    await Pelicula.update(req.body,{
        where:{id:req.params.peliculaId}
    });
    res.json('Pelicula Modificada');
});
//eliminar la pelicula 
router.delete('/:peliculaId', async (req, res) => {    
    await Pelicula.destroy({
        where: {
            id: req.params.peliculaId
        }
    })
    .then(function (deletedRecord) {
        if(deletedRecord === 1){
            res.status(200).json({message:"borrado satisfactorio"});          
        }
        else
        {
            res.status(404).json({message:"pelicula no encontrada"})
        }
    })
    .catch(function (error){
        res.status(500).json(error);
    });
});


module.exports = router;