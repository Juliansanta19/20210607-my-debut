// CAMBIAR NOMBRE POR index.js SI AÚN NO SE LLAMA ASÍ
const express = require("express");
const app = express();

const apiRouter = require("./routes/api")

require("./db")


app.use(express.json());
app.use(express.urlencoded({extended:true}));

// app.use("/api", apiRouter)

app.listen(3000,() => {
    console.log("Servidor corriendo por puerto 3000")
});