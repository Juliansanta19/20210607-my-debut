const express = require("express");

const app = express();

// Llamamos a nuestra base de datos

require("./src/database/db");
require("./src/database/sync");

//Configuramos nuestras rutas

//const apiRouter = require()

app.use(express.json());
app.use(express.urlencoded({extended:true}));


//Configuramos un path inicial

//app.use("/api", apiRouter);



app.listen(3000, () => {
    console.log("Servidor 3000")
});