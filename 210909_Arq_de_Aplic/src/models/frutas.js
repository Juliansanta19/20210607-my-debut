module.exports = (sequelize, type) => {

    return sequelize.define(
        "Frutas",
        {
            nombre: type.STRING,
            color: type.STRING,
            vitaminas: type.STRING,
        },{
            sequelize,
            timestamps: false, //para que no se incluyan en la DB dos carpetas automáticas
        }
    );
};

