const {Sequelize} = require("sequelize");
const {databaseAcamicadb} = require("./db");
const FrutasModelo = require("../models/frutas");

const Frutas = FrutasModelo(databaseAcamicadb, Sequelize);

databaseAcamicadb.sync({force: false}).then(() => {
    console.log("Tablas sincronizadas");
});

module.exports = {
    Frutas
}