const router = require("express").Router();
const {Frutas} = require("../../../database/sync");
const controller = require("../../../controllers/frutas");

//Listado de frutas

router.get("/", async (req, res) => {

    const frutas = await Frutas.findAll();

    res.json(frutas)
});


module.exports = router