/**
 * Funciones asincronas
 * 
 * Trabajar con código asíncrono complejo
 * Escribir un código basado en promesas > como si fuera sincronico
 * La palabra clave async va antes de la funcion 
 * La palabra await va antes de la instruccion que queremos que se ejecute
 * Await nos ayuda a  esperar a una promesa sea realizada antes de continuar con la ejecución de la función
 * async/await nos permite trabajar con promesas de forma sincronica
 * Super importante!!!: Las funciones asíncronas no devuelven datos, siempre devuelven promesas!!! atte el profe!
 * */


 const fetch = require('node-fetch');

//** el mismo ejemplo pero con f.asincrona

async function obtenerUsuario(){
    const response = await fetch("http://api.github.com/users/andrew");
    const data = await response.json();
    if(response.status === 200){
        //console.log(data)
        return data;
    }else{
        console.log(new Error("Se presento un error"));
    }
}

//si lo llamamos asi, solo nos devolvera que esta pendiente
//console.log(obtenerUsuario());
//para traer su informacion debemos usar then porque es una promesa
obtenerUsuario().then((data) => {
    console.log('la data es',data)
}).catch((error) => {
    console.log(error)
});


async function ObtenerPeliculas(){
    const response = await fetch('http://www.omdbapi.com/?t=angry birds&apikey=a2a4b086');
    const data =   await response.json();    
    if(data){
        return data;
    }else{
        return "mensaje error";
    }    
} 

ObtenerPeliculas().then(data => console.log(data));


/** Rules
 * 
 *The async function returns a promise. The converse is also true. Every function that returns a promise can be considered as async function.
await is used for calling an async function and waits for it to resolve or reject.
await blocks the execution of the code within the async function in which it is located.
If the output of function2 is dependent on the output of function1, I use await.
If two functions can be run in parallel, create two different async functions and then run them in parallel.
*/








// http://api.github.com/users/andrew
// http://www.omdbapi.com/?t=angry birds&apikey=a2a4b086
