function metodo_sincronico(){
    console.log("me gusta");
    console.log("programar");
    console.log("con cerveza");
    console.log("todos los dias");
    console.log("con mis perros");
};

function metodo_asincronico(){

    console.log("No me gusta");
    console.log("los días sin programar");

    setTimeout(() => {
        console.log("con agua");
    }, 3000);

    setTimeout(() => {
        console.log("sin cerveza");
    }, 7000);

    setTimeout(() => {
        console.log("SOLO LOS VIERNES");
    }, 2000);
    
    console.log("con mis perros");
    console.log("y con mis gatos");
};


// Promesa

let promesa_ejecucion = new Promise((resolve, reject) => {

    setTimeout(() => {
        resolve("Promesa resuelta en 3 segundos");
    },3000);

    setTimeout(() => {
        reject("Promesa rechazada");
    },1500);
});

// Atrapamos la promesa usando then y catch

promesa_ejecucion.then((mensaje) => {
    console.time("execution");
    console.log("respuesta es " + mensaje);
    console.timeEnd("execution");
}).catch((errorMessage) => {
    console.log("error de promesa " + errorMessage);
});


// Haciendo que entre en el rejecting

let promesa_rechazada = new Promise((resolve, reject) => {

    setTimeout(() => {
        resolve("Promesa ejecutada correctamente, seguro?")
    }, 1000);

    setTimeout(() => {
        reject("Promesa rechazada");
    }, 5000);
    
});

promesa_rechazada.then((mensaje) => {
    console.log(mensaje);
}).catch((error) => {
    console.log(error);
});


// ejemplo de promesa pendiente que queda por resolverse

let promesa_pendiente = new Promise((resolve, reject) => {

    console.log("Pendiente.......");

    setTimeout(() => {
        if(true){
            resolve("Promesa resuelta!");
        }else{
            reject("Promesa rechazada");
        }
    }, 3000);

});


promesa_pendiente.then((successMessage) => {
    
    console.log("Respuesta de promesa pendiente " + successMessage);
}).catch((errorMessage) => {

    console.log("Error de promesa pendiente " + errorMessage);
});


// Ejemplo de Fetch con promesa para obtener una imagen

let divImage = document.getElementById("imgDog");

function getDogImage(url){

    console.log("Pendiente para la imagen");

    fetch(url)
        .then(response => response.json())
        .then(json => {

            let dogImage = document.createElement("img");
            dogImage.setAttribute("src", json.message);
            dogImage.style.width = "300px";
            divImage.appendChild(dogImage);
            console.log("imagen cargada");
        })

        .catch(err => {
            console.log("Falló la comunicación al server " + err);
        });
};

getDogImage("https://dog.ceo/api/breeds/image/random");
metodo_sincronico();
metodo_asincronico();

// Otro ejemplo

const promesa_numero = new Promise((resolve, reject) =>{
    const numero = Math.floor(Math.random() * 10);

    setTimeout(() =>{
        numero > 4 ? resolve(numero) : reject(new Error("menor a 4: " + numero));
    }, 2000);
});


promesa_numero
    .then(numero => console.log(numero))
    .catch(error => console.error(error));

