//Promesa para obtener el listado de usuario de github
//En el siguiente ejercicio vamos a crear juntos una función que reciba como parámetro un usuario de GitHub
//y nos dé su información y cuáles son sus 5 primeros seguidores.

/*Traer información de la API de github*/
//Hacer fetch de la información de un usuario de github por nombre
//Hacer fetch de la información de los followers de ese usuario 
//(con el endpoint devuelto en el fetch anterior) y hacer console.log del nombre de usuario de los primeros 5 followers.


const fetch = require("node-fetch");

function getGithubUser(username){
    fetch("https://api.github.com/users/" + username)

    .then(response => response.json())
    .then(json => {
        console.log(json);
        getUserFirst5Followers(json.followers_url);
    }).catch(err => {
        console.error("fetch failed", err);
    });
};

function getUserFirst5Followers(url){

    fetch(url)
    .then(response => response.json())
    .then(jsondata => {
        console.log(jsondata);

        for(var i=0; i<5; i++){
            console.log(jsondata[i].login);
        }
    }).catch(err => {
        console.error("fetch failed", err);
    });
};

getGithubUser("andrew");