const apiKey = 'f92febfdd65a0b8a6cfd5bf8fb3ae7f0';

let searchInput = document.getElementById('search');
let searchBtn = document.getElementById('searchBtn');
let results = document.getElementById('results');

//Ejemplos de ciudades para el search query: London, Texas, Tokio, Seoul
//Tambien se puede buscar por país: Argentina, Uruguay, etc

function search(){
    //Fetch con async await
    async function newsSearch(city_name) {
        let url = `https://api.openweathermap.org/data/2.5/weather?q=${city_name}&appid=${apiKey}`;
        const resp = await fetch(url);
        const info = await resp.json();
        return info;
    }
    let info = newsSearch(searchInput.value);
    info.then(response => {      
        console.log(response);
        results.textContent = `City: ${response.name}, Clima: ${response.weather[0].main}, Temperatura:  ${response.main.temp} °K`
    }).catch(error => {
        console.log(error);
    })
}

searchBtn.addEventListener('click', ()=>{
    search();
})

