const csvtojson = require("csvtojson");
const mysql = require("mysql2");

//Data bse credentials

const hostname = "localhost",
username = "root",
password = "",
database = "acamicadb"

// Establecer conexión con la DB

let con = mysql.createConnection({
    host: hostname,
    user: username,
    password: password,
    database: database,
});

con.connect((err) => {

    if (err) return console.error(
        "error: " + err.message);

    con.query("DROP TABLE students",
    (err, drop) => {

        // query to create table sample
        var createStatement =
        "CREATE TABLE students(Name char(50), " +
        "Email char (50), Age int, City char(30)"

        //creating table sample

        con.query(createStatement, (err, drop) => {
            if (err)
            console.log("ERROR: ", err);
        })
    })
})

//csv file name

const fileName = "C:/Users/Julián/Desktop/ACÁMICA/20210607 My Debut/210830_MySQL_III/data.csv";

csvtojson().fromFile(fileName).then(source => {

    //Fetching the data from each row
    // and inserting to the TABLE sample

    for (var i = 0; i < source.length; i++){
        var Name = source[i]["Name"],
        Email = source[i]["Email"]
        Age = source[i]["Age"]
        City = source[i]["City"]

        var instertStatement = 
        `INSERT INTO students values(?, ?, ?, ?)`;

        var items = [Name, Email, Age, City];

        // Inserting data of current row
        //into DB

        con.query(instertStatement, items,
            (err, results, fields) => {
                if (err) {
                    console.log("No se puede insertar el registro en el item ", i +1);
                    return console.log(err);
                };
            });
    };

    console.log("Todos los items fueron almacenados satisfactoriamente");
});