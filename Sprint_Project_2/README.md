----------------------------------------------------- DELILAH RESTÓ - API 1.0.0 -----------------------------------------------------

# INTRODUCCIÓN

Esta API fue desarrollada para uso exclusivo de los responsables de la empresa Delilah Restó, con soporte del equipo de desarrollo de ACÁMICA.

El proyecto tiene como objetivo brindar al cliente una solución que le permita centralizar tanto los productos que desea comercializar como los pedidos que se realizarán por medio de ella, además de los datos de los usuarios que se registren para tal fin.

Para la visualización de los productos y la realización de los pedidos será necesario que cada usuario se registre previamente. El correspondiente registro cuenta con un sistema de validación que detecta usuarios y correos duplicados (los cuales no serán permitidos para registrarse), además de corroborar que todos los campos hayan sido completados.

Una vez registrado, el usuario podrá loguearse en el sistema y ver los productos disponibles, agregarlos a su carrito (o eliminarlos), hacer el pedido de dichos productos y ver su historial de pedidos. Además, podrán ver los medios de pago habilitados por la empresa.

Por su parte, los usuarios Administradores podrán llevar adelante los mismos ítems, pero además estarán habilitados para agregar, modificar y eliminar productos de la base de datos (dependiendo de su stock); agregar, modificar y eliminar medios de pago; ver el historial de todos los pedidos solicitados a Delilah Restó y cambiar el estado de cada uno de ellos.


## INSTALACIÓN

Esta API fue desarrollada en Node.js. El server está configurado predeterminadamente para salir por el puerto 3000, por lo cual al final del código podrán asignar el número de puerto que se adapte a sus necesidades.

Se recomienda la inicialización de NPM y la posterior instalación de Express, Nodemon, Swagger y Moment, los cuales fueron las librerías utilizadas para esta API.

Cabe destacar que cada uno de los endpoint fue testeado por Postman y documentado con Swagger para el correspondiente acceso del cliente.


### DESARROLLO

El código fue divido en distintos apartados: BASE DE DATOS, MIDDLEWARES, USUARIOS, PRODUCTOS, PEDIDOS, MEDIOS DE PAGO y PUERTO. 

A excepción de los endpoints de usuarios, el resto de los endpoint tomará desde el req.params el ID del usuario logueado para validar no sólo que haya hecho el login, sino que también validará si es administrador o no según sea necesario.

- BASE DE DATOS: Aquí se incluyen los array que contendrán la información de los usuarios (users), productos (products), pedidos (orders), estados de envío (payMethod) y estados de pedido (orderStatus).
El array de usuarios cuenta ya con dos usuarios creados, uno de Administrador y uno que simula a un usuario común; el array de productos cuenta con 3 pre cargados; el de pedidos cuenta con una orden con todos sus campos completos como "PRUEBA"; medios de pago figura con 4 medios dados de alta; y los 5 estados de pedido ya fueron creados según fue solicitado por el cliente en el requerimiento.

- MIDDLEWARES: Se crearon dos middlewares de validación; uno valida que sólo el usuario logueado pueda acceder a ciertos endpoints y el otro valida que sólo los administradores puedan acceder a otros.

- USUARIOS: Los endpoints de usuarios están destinados al registro de nuevos usuarios y al login.

- PRODUCTOS: Los endpoints de productos permiten visualizar los productos disponibles, agregar, modificar y eliminar productos, previa validación de Administrador según corresponda.

- PEDIDOS: Cada usuario podrá agregar y quitar productos a su carrito y luego proceder a iniciar el pedido indicando el medio de pago deseado. Para agregar productos al carrito debe indicar el ID del producto y la cantidad que desea llevar; luego, para seleccionar la forma de pago, cada medio será tomado según su índice dentro del array: 0 = EFECTIVO; 1 = DÉBITO; 2 = CRÉDITO; 3 = MERCADO PAGO. De todas formas, cada medio de pago cuenta también con un ID en caso de que en un futuro se desee cambiar y tomar desde el ID en vez de su índice. Una vez completado el pedido, se le asignará predeterminadamente el estado "Pendiente" y el carrito será vaciado para próximos pedidos. El usuario también podrá ver su historial de pedidos y chequear el estado de los mismos.
Por su parte, los usuarios administradores podrán ver el historial de pedidos de todos los usuarios y modificar el estado de los mismos, indicando su posición dentro del array de orderStatus: 0 = PENDIENTE; 1 = CONFIRMADO; 2 = EN PREPARACIÓN; 3 = ENVIADO; 4 = ENTREGADO.

- MEDIOS DE PAGO: Todos los usuarios podrán visualizar los medios de pago habilitados. Los administradores podrán dar de alta, modificar o eliminar medios de pago.

- PUERTO: El último apartado del código contiene el puerto desde donde el servidor está corriendo.


------------------------------------------------------------------------------------------------------------------------------------

