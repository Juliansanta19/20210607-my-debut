const compression = require("compression")
const express = require('express');
const server = express();
const moment = require("moment");
const now = moment(new Date());


const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
//configuracion de middleware para aceptar json en su formato 
server.use(express.json()) // for parsing application/json
server.use(express.urlencoded({ extended: true })) // for parsing application/x-   www-form-urlencoded
//configurar el option
const swaggerOptions =  {
    swaggerDefinition:{
        info: {
            title: 'Delilah Restó - My Primera API',
            version:'1.0.0'
        }
    },
    apis:['./server.js']
};
//configurar el swaggerDocs
const swaggerDocs = swaggerJsDoc(swaggerOptions);
//configuro el server para el uso del swagger, o configurar el endpoint
server.use('/api-docs', 
            swaggerUI.serve,
            swaggerUI.setup(swaggerDocs));