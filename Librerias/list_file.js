const fs = require("fs");

const folderPath = "./";

//Mostrar el listado de los archivos de la carpeta

function mostrar_archivos(){
    fs.readdir(folderPath, (err, files) => {
        files.forEach(file => {
            console.log(file)
        });
    });
}

exports.mostrar_archivos = mostrar_archivos;
