/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
 *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success * 
 */




/**
 * @swagger
 * /estudiantes:
 *  put:
 *    description: Actualizar un estudiante
 *    responses:
 *      200:
 *        description: Success * 
 */