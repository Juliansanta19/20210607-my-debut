const mongoose = require("./conexion");
const express = require("express");
const app = express();
app.listen(4000, function () {
  console.log("listening on 4000");
});
async function insertarUsuario() {
  const userSchema = mongoose.Schema({
    nombre: String,
    apellido: String,
    edad: Number,
  });
  //crea una instancia del modelo de mongodb
  const Usuarios = mongoose.model("Usuarios", userSchema);
  //instanciamos la sesion
  const session = await mongoose.startSession();
  //empezamos la transaccion
  session.startTransaction();
  //llamados al metodo create para agregar un nuevo documento
  await Usuarios.create(
    [{ nombre: "Luis T", apellido: "Rodriguez", edad: 50 }],
    { session: session }
  );
  await Usuarios.create(
    [{ nombre: "Maria 2", apellido: "Ruiz", edad: 35 }],
    { session: session }
  );
  //consultamos el documento creado
  //let usuarioDoc = await Usuarios.findOne({ nombre: "Prueba T" });
  //assert.ok(!usuarioDoc);
  ///una vez que se hace el commit de la transaccion el documento es visible fuera de ella
  await session.commitTransaction();
  //si deseo abortar el la session mongodb
  // Al indicarle que aborte
  //await session.abortTransaction();
  //usuarioDoc = await Usuarios.findOne({ nombre: "Prueba T" });
  //assert.ok(!usuarioDoc);
  //terminar la session del document
  session.endSession();
  //Usuarios.find().then((result) => console.log('nombre es ' + result[0].nombre));
  mongoose.model('Usuarios').find(function(err, users){
    users.forEach(function(user) {
      console.log(user.nombre);
      console.log(user.apellido);      
    });    
  });
}
insertarUsuario();