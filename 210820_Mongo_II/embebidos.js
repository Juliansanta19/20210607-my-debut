const mongoose = require("./conexion");
const express = require("express");
const { Schema } = mongoose;
const app = express();
app.listen(3500, function () {
  console.log("listening on 3500");
});
function insertarPosteo() {
    const Comentario = new Schema({
        titulo:String,
        cuerpo:String,
        fecha:Date
    });
    const Posteo = new Schema({
        autor:mongoose.ObjectId,
        titulo: String,
        cuerpo:String,
        fecha: Date,
        comentarios:[Comentario],//documento embebido se añade en forma de array
        meta :{
            votos:Number,
            favs: Number
        }
    });
    //modelo Posteo
    const mdlPosteo = mongoose.model('Posteo',Posteo);
    const objPosteo = new mdlPosteo({
        titulo: "Hola Mundo!",
        cuerpo:"Este es el cuerpo del documento",
        fecha: new Date(),
    });
    objPosteo.comentarios.push({titulo:'Esto es un documento embebido, comentario de prueba!!!', cuerpo:"Cuerpo de prueba", fecha:new Date()});
    objPosteo.save((err) => {
        if(!err) console.log('Guardado'); 
    });
}
function eliminarPosteo() {
    const mdlPosteo = mongoose.model('Posteo',Posteo);
    mdlPosteo.findById('612018b74cdb3f45900c9599',(err, post) =>{
        if(!err) {
            post.comentarios[0].remove();
            post.save((err) =>{
                if(!err) console.log('Eliminado')
            });
        }
    });
}
function encontrarPosteo() {
    //modelo Posteo
    const mdlPosteo = mongoose.model('Posteo');   
    mdlPosteo.findById('6120317de4dbf35a6073976d',(err, post) =>{
        if(!err) {
            console.log('comentario',post.comentarios[0].titulo);          
        }
    });
}
//insertarPosteo();
//eliminarPosteo();
//encontrarPosteo();